package com.techcraeft.kinodaran.di

import com.example.armboldmindtest.data.converters.implementation.ProductConverter
import com.example.armboldmindtest.data.converters.implementation.ProductDetailConverter
import com.techcraeft.kinodaran.data.converters.DtoUIModelConverterContainer
import com.techcraeft.kinodaran.data.converters.implementation.CategoryConverter
import org.koin.core.qualifier.named
import org.koin.dsl.module

val converterModule = module {

    single {
        DtoUIModelConverterContainer(
            get(named(NAME_HOME_CONVERTER)),get(),get())
    }

    single { CategoryConverter() }
    single { ProductConverter() }
    single { ProductDetailConverter() }
}

private const val NAME_HOME_CONVERTER = "home_converter"
