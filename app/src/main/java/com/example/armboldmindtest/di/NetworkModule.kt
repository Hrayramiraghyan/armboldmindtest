package com.techcraeft.kinodaran.di

import com.example.armboldmindtest.BuildConfig
import com.techcraeft.kinodaran.data.network.api.HomeApi
import com.techcraeft.kinodaran.data.network.util.RequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    single { RequestInterceptor() }
    single { provideRetrofitClient(get(), HomeApi::class.java) }
    single { provideOkHttpClient(get(), get()) }
}

private fun <T> provideRetrofitClient(okHttpClient: OkHttpClient, clazz: Class<T>): T {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
        .create(clazz)
}

private fun provideOkHttpClient(
    loggingInterceptor: HttpLoggingInterceptor,
    requestInterceptor: RequestInterceptor
): OkHttpClient {
    return OkHttpClient()
        .newBuilder()
        .addInterceptor(requestInterceptor)
        .addNetworkInterceptor(loggingInterceptor)
        .build()
}

