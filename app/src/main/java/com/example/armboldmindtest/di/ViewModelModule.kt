package com.techcraeft.kinodaran.di

import com.example.armboldmindtest.presenter.viewmodels.HomeViewModel
import com.example.armboldmindtest.presenter.viewmodels.ProductDetailsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { HomeViewModel(get()) }
    viewModel { ProductDetailsViewModel(get()) }
}