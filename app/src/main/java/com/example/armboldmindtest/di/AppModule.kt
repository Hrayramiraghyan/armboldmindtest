package com.techcraeft.kinodaran.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.techcraeft.kinodaran.manager.HomeManager
import com.techcraeft.kinodaran.manager.impl.HomeManagerImpl
import org.koin.dsl.module

val appModule = module {
    factory { provideGson() }

    single<HomeManager> {
        HomeManagerImpl(
            get()
        )
    }
}

private fun provideGson(): Gson {
    return GsonBuilder().create()
}