package com.techcraeft.kinodaran.di

import com.example.armboldmindtest.repository.HomeRepository
import com.example.armboldmindtest.repository.impl.HomeRepositoryImpl
import com.techcraeft.kinodaran.repository.util.ResponseHandler
import org.koin.dsl.module

val repositoryModule = module {
    single { ResponseHandler() }
    single<HomeRepository> { HomeRepositoryImpl(get(), get(),get(),get(),get()) }
}