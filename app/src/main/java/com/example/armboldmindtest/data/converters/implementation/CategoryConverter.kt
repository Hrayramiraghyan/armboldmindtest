package com.techcraeft.kinodaran.data.converters.implementation

import com.example.armboldmindtest.data.network.dto.CategoriesDto
import com.example.armboldmindtest.model.Categories
import com.techcraeft.kinodaran.data.converters.contracts.DtoUIModelConverter

typealias CategoryDtoUIModelConverter = DtoUIModelConverter<CategoriesDto, Categories>

class CategoryConverter : CategoryDtoUIModelConverter {

    override fun dtoToModel(dtoObject: CategoriesDto): Categories {
        return Categories(
            dtoObject.success,
            dtoObject.data,
            dtoObject.message
        )
    }

    override fun modelToDto(model: Categories): CategoriesDto {
        TODO("Not yet implemented")
    }
}