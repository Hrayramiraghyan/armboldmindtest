package com.techcraeft.kinodaran.data.converters.contracts

interface DtoUIModelConverter<T, R> {
    fun dtoToModel(dtoObject: T): R
    fun modelToDto(model: R): T
}