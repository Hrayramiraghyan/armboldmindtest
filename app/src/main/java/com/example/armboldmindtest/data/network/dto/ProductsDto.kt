package com.example.armboldmindtest.data.network.dto

import com.example.armboldmindtest.model.Product

data class ProductsDto(
    val success: Boolean,
    val data: List<Product>,
    val message: String
)