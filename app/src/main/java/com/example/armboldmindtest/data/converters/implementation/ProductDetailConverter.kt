package com.example.armboldmindtest.data.converters.implementation

import com.example.armboldmindtest.data.network.dto.ProductDetailsDto
import com.example.armboldmindtest.model.ProductDetails
import com.techcraeft.kinodaran.data.converters.contracts.DtoUIModelConverter

typealias ProductDetailtoUIModelConverter = DtoUIModelConverter<ProductDetailsDto, ProductDetails>

class ProductDetailConverter : ProductDetailtoUIModelConverter {

    override fun dtoToModel(dtoObject: ProductDetailsDto): ProductDetails {
        return ProductDetails(
            dtoObject.success,
            dtoObject.data,
            dtoObject.message
        )
    }

    override fun modelToDto(model: ProductDetails): ProductDetailsDto {
        TODO("Not yet implemented")
    }
}