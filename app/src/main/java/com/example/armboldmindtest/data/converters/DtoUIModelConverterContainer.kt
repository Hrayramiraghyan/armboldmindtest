package com.techcraeft.kinodaran.data.converters

import com.example.armboldmindtest.data.converters.implementation.ProductDetailtoUIModelConverter
import com.example.armboldmindtest.data.converters.implementation.ProductDtoUIModelConverter
import com.techcraeft.kinodaran.data.converters.implementation.CategoryDtoUIModelConverter

data class DtoUIModelConverterContainer(
    val categoryDtoUIModelConverter: ProductDtoUIModelConverter,
    val productDtoUIModelConverter: CategoryDtoUIModelConverter,
    val productDetailDtoUIModelConverter: ProductDetailtoUIModelConverter
)