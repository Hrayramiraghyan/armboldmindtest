package com.example.armboldmindtest.data.converters.implementation

import com.example.armboldmindtest.data.network.dto.ProductsDto
import com.example.armboldmindtest.model.Products
import com.techcraeft.kinodaran.data.converters.contracts.DtoUIModelConverter

typealias ProductDtoUIModelConverter = DtoUIModelConverter<ProductsDto, Products>

class ProductConverter : ProductDtoUIModelConverter {

    override fun dtoToModel(dtoObject: ProductsDto): Products {
        return Products(
            dtoObject.success,
            dtoObject.data,
            dtoObject.message
        )
    }

    override fun modelToDto(model: Products): ProductsDto {
        TODO("Not yet implemented")
    }
}