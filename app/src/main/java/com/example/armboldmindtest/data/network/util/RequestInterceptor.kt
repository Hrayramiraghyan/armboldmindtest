package com.techcraeft.kinodaran.data.network.util

import okhttp3.Interceptor
import okhttp3.Response

class RequestInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()

        if (!token.isNullOrEmpty()) {
            val finalToken = "Bearer " + token
            request = request.newBuilder()
                .addHeader("Authorization", finalToken)
                .addHeader("languageName", en)
                .build()
        }
        return chain.proceed(request)
    }

    companion object {
        const val token: String =
            "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJDMTNDNkM2OUMwRTQ0NEFDQTEzNDAxRUUxQ0JGQTRBQSIsInNjb3BlcyI6IlJPTEVfR1VFU1QiLCJpYXQiOjE1OTQ4MTkyMDksImV4cCI6MTYyNjM1NTIwOX0.99jvQj3s26aSjCYf9SIqdiD4Hi-DhPcPy3Ar_e5mu48"
        const val en: String = "en"
    }
}