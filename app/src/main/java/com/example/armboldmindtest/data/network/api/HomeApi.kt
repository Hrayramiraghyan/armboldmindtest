package com.techcraeft.kinodaran.data.network.api

import com.example.armboldmindtest.data.network.dto.CategoriesDto
import com.example.armboldmindtest.data.network.dto.ProductDetailsDto
import com.example.armboldmindtest.data.network.dto.ProductsDto
import retrofit2.http.GET
import retrofit2.http.Query

interface HomeApi {

    @GET("category/getTopCategoriesForMobile")
    suspend fun getTopCategories(): CategoriesDto

    @GET("product/getMostPopularProducts")
    suspend fun getMostPopularProducts(): ProductsDto

    @GET("product/getProductById")
    suspend fun getProductById(@Query("id") productId: Int): ProductDetailsDto
}