package com.example.armboldmindtest.data.network.dto

import com.example.armboldmindtest.model.Product

class ProductDetailsDto(
    val success: Boolean,
    val data: Product,
    val message: String
)