package com.example.armboldmindtest.data.network.dto

import com.example.armboldmindtest.model.Category

data class CategoriesDto(
    val success: Boolean,
    val data: List<Category>,
    val message: String
)