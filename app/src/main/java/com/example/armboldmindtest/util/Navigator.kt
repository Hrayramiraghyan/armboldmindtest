package com.example.armboldmindtest.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.armboldmindtest.R
import com.example.armboldmindtest.model.Product
import com.example.armboldmindtest.presenter.activities.MainActivity
import com.example.armboldmindtest.presenter.fragments.ProductDetailsFragment
import com.techcraeft.kinodaran.presenter.fragments.*

open class Navigator {

    fun navigateToHome(activity: Fragment) {
        activity
            .childFragmentManager
            .beginTransaction()
            .add(R.id.nav_home_container, HomeFragment.newInstance())
            .commit()
    }

    fun navigateToSearch(activity: Fragment) {
        activity
            .childFragmentManager
            .beginTransaction()
            .add(R.id.nav_search_container, SearchFragment.newInstance())
            .commit()
    }

    fun navigateToNotification(activity: Fragment) {
        activity
            .childFragmentManager
            .beginTransaction()
            .add(R.id.nav_upcoming_container, NotificationFragment.newInstance())
            .commit()
    }

    fun navigateToProductDetails(activity: FragmentActivity, product: Product) {
        getNavHomeFragment(activity)
            .childFragmentManager
            .beginTransaction()
            .add(R.id.nav_home_container, ProductDetailsFragment.newInstance(product))
            .addToBackStack(null)
            .commit()
    }

    fun getNavHomeFragment(activity: FragmentActivity) =
        activity.supportFragmentManager.findFragmentByTag(MainActivity.NAV_HOME_TAG) as Fragment
}