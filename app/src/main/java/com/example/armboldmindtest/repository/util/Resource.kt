package com.techcraeft.kinodaran.repository.util

data class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val exception: Exception?
) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null,
                null
            )
        }

        fun <T> error(exception: Exception?, msg: String?, data: T?): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                msg,
                exception
            )
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                msg,
                null
            )
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(
                Status.LOADING,
                data,
                null,
                null
            )
        }
    }
}