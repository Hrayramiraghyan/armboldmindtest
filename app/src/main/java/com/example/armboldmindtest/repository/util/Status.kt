package com.techcraeft.kinodaran.repository.util

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}