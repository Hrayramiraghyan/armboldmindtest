package com.example.armboldmindtest.repository.impl

import com.example.armboldmindtest.data.converters.implementation.ProductConverter
import com.example.armboldmindtest.data.converters.implementation.ProductDetailConverter
import com.example.armboldmindtest.model.Categories
import com.example.armboldmindtest.model.ProductDetails
import com.example.armboldmindtest.model.Products
import com.example.armboldmindtest.repository.HomeRepository
import com.techcraeft.kinodaran.data.converters.implementation.CategoryConverter
import com.techcraeft.kinodaran.data.network.api.HomeApi
import com.techcraeft.kinodaran.repository.util.Resource
import com.techcraeft.kinodaran.repository.util.ResponseHandler

class HomeRepositoryImpl(
    private val homeApi: HomeApi,
    private val categoryConverter: CategoryConverter,
    private val productConverter: ProductConverter,
    private val productDetailConverter: ProductDetailConverter,
    private val responseHandler: ResponseHandler
) : HomeRepository {

    override suspend fun getTopCategories(): Resource<Categories> {
        return try {
            val result = homeApi.getTopCategories()
            responseHandler.handleSuccess(categoryConverter.dtoToModel(result))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    override suspend fun getMostPopularProducts(): Resource<Products> {
        return try {
            val result = homeApi.getMostPopularProducts()
            responseHandler.handleSuccess(productConverter.dtoToModel(result))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }

    override suspend fun getProductById(productId: Int): Resource<ProductDetails> {
        return try {
            val result = homeApi.getProductById(productId)
            responseHandler.handleSuccess(productDetailConverter.dtoToModel(result))
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }


}