package com.example.armboldmindtest.model

data class Category(
    val id: Int,
    val categoryName: String?,
    val productCount: Int,
    val imageUrl: String?
)
