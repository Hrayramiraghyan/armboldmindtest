package com.example.armboldmindtest.model

data class Products(
    val success: Boolean,
    val data: List<Product>,
    val message: String
)