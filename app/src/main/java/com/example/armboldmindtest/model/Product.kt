package com.example.armboldmindtest.model

import java.io.Serializable

data class Product(
    val id: Int,
    val productName: String?,
    val description: String?,
    val ingredients: String?,
    val shelfLife: String?,
    val price: Int,
    val measurementEnumValue: Int,
    val imageUrl: String?,
    val categoryNames: List<String>,
    val favorite: Boolean
) : Serializable