package com.example.armboldmindtest.model

data class ProductDetails(
    val success: Boolean,
    val data: Product,
    val message: String

)