package com.example.armboldmindtest.model

data class Categories(
    val success: Boolean,
    val data: List<Category>,
    val message: String
)