package com.example.armboldmindtest.presenter.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.armboldmindtest.model.Product
import com.example.armboldmindtest.model.ProductDetails
import com.example.armboldmindtest.repository.HomeRepository
import com.techcraeft.kinodaran.repository.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductDetailsViewModel(
    private val repository: HomeRepository
) : ViewModel() {

    private val productDetails: MutableLiveData<Resource<ProductDetails>> = MutableLiveData()

    fun getProductDetails(): LiveData<Resource<ProductDetails>> {
        return productDetails
    }

    fun setSelectedProduct(product: Product) {
        productDetails.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            val data = repository.getProductById(product.id)
            withContext(Dispatchers.Main) {
                productDetails.value = data
            }
        }
    }
}
