package com.example.armboldmindtest.presenter.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.armboldmindtest.R
import com.techcraeft.kinodaran.presenter.fragments.NavHomeFragment
import com.techcraeft.kinodaran.presenter.fragments.NavNotificationFragment
import com.techcraeft.kinodaran.presenter.fragments.NavSearchFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var containerFragments = mutableMapOf<String, Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            containerFragments.clear()

            containerFragments[NAV_HOME_TAG] = NavHomeFragment.newInstance()
            containerFragments[NAV_SEARCH_TAG] = NavSearchFragment.newInstance()
            containerFragments[NAV_NOTIFICATION_TAG] = NavNotificationFragment.newInstance()


            supportFragmentManager.beginTransaction()
                .add(
                    R.id.main_container,
                    containerFragments[NAV_HOME_TAG]!!,
                    NAV_HOME_TAG
                )
                .add(
                    R.id.main_container,
                    containerFragments[NAV_SEARCH_TAG]!!,
                    NAV_SEARCH_TAG
                ).hide(containerFragments[NAV_SEARCH_TAG]!!)
                .add(
                    R.id.main_container,
                    containerFragments[NAV_NOTIFICATION_TAG]!!,
                    NAV_NOTIFICATION_TAG
                ).hide(containerFragments[NAV_NOTIFICATION_TAG]!!)


                .commit()
        } else {
            containerFragments.clear()

            containerFragments[NAV_HOME_TAG] =
                supportFragmentManager.findFragmentByTag(NAV_HOME_TAG)!!
            containerFragments[NAV_SEARCH_TAG] =
                supportFragmentManager.findFragmentByTag(NAV_SEARCH_TAG)!!

            containerFragments[NAV_NOTIFICATION_TAG] =
                supportFragmentManager.findFragmentByTag(NAV_NOTIFICATION_TAG)!!

        }

        initBottomNavigation()
    }

    private fun initBottomNavigation() {
        nav_view.setOnNavigationItemSelectedListener {
            return@setOnNavigationItemSelectedListener when (it.itemId) {
                R.id.nav_home -> swapFragment(
                    NAV_HOME_TAG
                )
                R.id.nav_search -> swapFragment(
                    NAV_SEARCH_TAG
                )
                R.id.nav_notification -> swapFragment(
                    NAV_NOTIFICATION_TAG
                )
                else -> false
            }
        }
    }

    private fun swapFragment(tag: String): Boolean {
        val fragment = containerFragments[tag]!!

        supportFragmentManager.beginTransaction()
            .apply {
                containerFragments.forEach {
                    if (it.key != tag) {
                        this.hide(it.value)
                    }
                }
            }
            .show(fragment)
            .commit()

        return true
    }

    override fun onBackPressed() {
        val currentNavBaseFragment =
            when (nav_view.selectedItemId) {
                R.id.nav_home -> containerFragments[NAV_HOME_TAG]
                else -> null
            }

        val currentFragment = currentNavBaseFragment

        if (currentFragment != null) {
            if (currentFragment.childFragmentManager.backStackEntryCount > 0) {
                currentFragment.childFragmentManager.popBackStack()
            } else {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        const val NAV_HOME_TAG = "navHome"
        const val NAV_SEARCH_TAG = "navSearch"
        const val NAV_NOTIFICATION_TAG = "navNotification"

    }
}