package com.example.armboldmindtest.presenter.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.armboldmindtest.model.Categories
import com.example.armboldmindtest.model.Products
import com.techcraeft.kinodaran.manager.HomeManager
import com.techcraeft.kinodaran.repository.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(private val homeManager: HomeManager) : ViewModel() {

    private val topCategories: MutableLiveData<Resource<Categories>> = MutableLiveData()
    private val popularProducts: MutableLiveData<Resource<Products>> = MutableLiveData()

    fun getTopCategories(): LiveData<Resource<Categories>> {
        return topCategories
    }

    fun getPopularProducts(): LiveData<Resource<Products>> {
        return popularProducts
    }

    init {
        loadTopCategories()
        getMostPopularProducts()
    }

    fun loadTopCategories() {
        topCategories.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            val data = homeManager.getTopCategories()
            withContext(Dispatchers.Main) {
                topCategories.value = data
            }
        }
    }

    fun getMostPopularProducts() {
        popularProducts.value = Resource.loading(null)
        viewModelScope.launch(Dispatchers.IO) {
            val data = homeManager.getMostPopularProducts()
            withContext(Dispatchers.Main) {
                popularProducts.value = data
            }
        }
    }

}