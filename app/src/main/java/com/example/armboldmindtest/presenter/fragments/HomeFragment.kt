package com.techcraeft.kinodaran.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.armboldmindtest.R
import com.example.armboldmindtest.adapter.CategoryAdapter
import com.example.armboldmindtest.adapter.ProductAdapter
import com.example.armboldmindtest.model.Category
import com.example.armboldmindtest.model.Product
import com.example.armboldmindtest.presenter.viewmodels.HomeViewModel
import com.example.armboldmindtest.util.Constants
import com.example.armboldmindtest.util.Navigator
import com.techcraeft.kinodaran.repository.util.Status
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class HomeFragment : BaseFragment() {

    private val vmHome: HomeViewModel by sharedViewModel()

    private val listener: ProductAdapter.ProductInteractionListener = object :
        ProductAdapter.ProductInteractionListener {

        override fun onProductSelected(position: Int, product: Product) {
            navigateToProductDetails(product)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vmHome.getTopCategories().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                        pb_loading.visibility = View.VISIBLE
                    }
                Status.SUCCESS -> it.data?.let { categories ->
                    pb_loading.visibility = View.GONE
                    initCategoryList(categories.data)
                    tv_category.visibility = View.VISIBLE
                }
                Status.ERROR -> {
                    //handle error
                }
            }
        })

        vmHome.getPopularProducts().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> { }
                Status.SUCCESS -> it.data?.let { products ->
                    initProductList(products.data)
                    tv_most_popular.visibility = View.VISIBLE

                }
                Status.ERROR -> {
                    //handle error
                }
            }
        })

        initListeners()
    }

    private fun initCategoryList(list: List<Category>) {
        rv_categories.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv_categories.adapter = CategoryAdapter(list)
    }

    private fun initProductList(list: List<Product>) {
        rv_products.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_products.adapter = ProductAdapter(list, listener)
    }

    private fun navigateToProductDetails(product: Product) {
        Navigator().navigateToProductDetails(requireActivity(), product)
    }

    private fun initListeners() {
        rv_categories.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dx > 0) {
                    Constants.IMAGE_ROTATION_ANGLE++
                } else {
                    Constants.IMAGE_ROTATION_ANGLE--
                }
                rv_categories.adapter?.notifyDataSetChanged()
            }
        })
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

}