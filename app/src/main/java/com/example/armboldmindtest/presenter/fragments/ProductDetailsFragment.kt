package com.example.armboldmindtest.presenter.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.armboldmindtest.R
import com.example.armboldmindtest.di.GlideApp
import com.example.armboldmindtest.model.Product
import com.example.armboldmindtest.presenter.viewmodels.ProductDetailsViewModel
import com.example.armboldmindtest.util.Constants
import com.techcraeft.kinodaran.presenter.fragments.BaseFragment
import com.techcraeft.kinodaran.repository.util.Status
import kotlinx.android.synthetic.main.fragment_product_details.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ProductDetailsFragment : BaseFragment() {

    private val vmProductDetails: ProductDetailsViewModel by sharedViewModel()

    companion object {
        @JvmStatic
        fun newInstance(product: Product) = ProductDetailsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(Constants.ARG_PRODUCT, product)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vmProductDetails.getProductDetails().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.LOADING -> {
                    pb_loading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> it.data?.let { productDetails ->
                    pb_loading.visibility = View.GONE
                    updateViews(productDetails.data)
                }
                Status.ERROR -> {

                }
            }
        })

        if (savedInstanceState == null) {
            initProduct()
        }
        initListeners()
    }

    private fun initProduct() {
        arguments?.let {
            if (it.containsKey(Constants.ARG_PRODUCT)) {
                val product = it.getSerializable(Constants.ARG_PRODUCT) as Product
                updateViews(product)
                vmProductDetails.setSelectedProduct(product)
            }
        }
    }

    private fun initListeners(){
        iv_back.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                activity?.onBackPressed()
            }
        })
    }

    private fun updateViews(product: Product) {

        if (!product.categoryNames.isNullOrEmpty()) {
            tv_category_name.text = product.categoryNames.get(0)
        } else {
            tv_category_name.text = ""
        }

        iv_product_image.displayImage(product.imageUrl)
        tv_produt_name.text = product.productName
        tv_produt_price.text = product.price.toString() + " AMD"
        tv_product_description.text = product.description
        tv_product_ingredient.text = product.ingredients
        tv_shelf_life.text = product.shelfLife
    }

    fun ImageView.displayImage(url: String?) {
        GlideApp.with(context)
            .setDefaultRequestOptions(
                RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .fitCenter()
            )
            .load(url)
            .error(R.drawable.ic_placeholder)
            .placeholder(R.drawable.ic_placeholder)
            .into(this)
    }

}

