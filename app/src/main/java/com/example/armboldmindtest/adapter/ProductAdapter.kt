package com.example.armboldmindtest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.TranslateAnimation
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.armboldmindtest.R
import com.example.armboldmindtest.di.GlideApp
import com.example.armboldmindtest.model.Product
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.android.synthetic.main.item_product.view.iv_basket

class ProductAdapter(
    items: List<Product>,
    val listener: ProductInteractionListener
) : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private val products = mutableListOf<Product>()

    init {
        products.addAll(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_product,
                parent,
                false
            )
        )
    }

    interface ProductInteractionListener {
        fun onProductSelected(position: Int, product: Product)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        (holder).bindProduct(position)
    }

    override fun getItemCount(): Int {
        return products.size; }

    fun getItem(position: Int): Product = products[position]

    inner class ProductViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindProduct(position: Int) {
            val product = getItem(position)
            view.img_product_icon.displayImage(product.imageUrl)
            view.tv_product_title.text = product.productName
            view.txt_product_price.text = product.price.toString().plus("֏/kg")
            view.iv_basket.setOnClickListener {
                val anim = TranslateAnimation(0F, 610F, 0F, -80F)
                anim.duration = 3000
                view.iv_basket.startAnimation(anim)
            }
            view.setOnClickListener {
                listener.onProductSelected(position, product)
            }
        }

        fun ImageView.displayImage(url: String?) {
            GlideApp.with(context)
                .setDefaultRequestOptions(
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .centerCrop()
                )
                .load(url)
                .error(R.drawable.ic_placeholder)
                .placeholder(R.drawable.ic_placeholder)
                .into(this)
        }
    }
}