package com.example.armboldmindtest.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.armboldmindtest.R
import com.example.armboldmindtest.di.GlideApp
import com.example.armboldmindtest.model.Category
import com.example.armboldmindtest.util.Constants
import kotlinx.android.synthetic.main.item_category.view.*

class CategoryAdapter(
    items: List<Category>
) : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private val categories = mutableListOf<Category>()

    init {
        categories.addAll(items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_category,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        (holder).bindCategory(position)
    }

    override fun getItemCount(): Int {
        return categories.size; }

    fun getItem(position: Int): Category = categories[position]

    inner class CategoryViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun bindCategory(position: Int) {
            val numbers = intArrayOf(
                R.drawable.category_gradient_pink_bg,
                R.drawable.category_gradient_green_bg,
                R.drawable.category_gradient_orange_bg,
                R.drawable.category_gradient_blue_bg,
                R.drawable.category_gradient_red_bg
            )

            val category = getItem(position)
            view.iv_category_image.displayImage(category.imageUrl)
            view.tv_category_title.text = category.categoryName
            val prodcutsInfo = category.productCount.toString() + " products"
            view.tv_product_count.text = prodcutsInfo
            view.cl_container.setBackgroundResource(numbers[position])
            view.iv_category_image.rotation = Constants.IMAGE_ROTATION_ANGLE
        }

        fun ImageView.displayImage(url: String?) {
            GlideApp.with(context)
                .setDefaultRequestOptions(
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .fitCenter()
                )
                .load(url)
                .into(this)
        }
    }
}