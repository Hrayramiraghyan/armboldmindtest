package com.techcraeft.kinodaran.manager.impl

import com.example.armboldmindtest.model.Categories
import com.example.armboldmindtest.model.ProductDetails
import com.example.armboldmindtest.model.Products
import com.example.armboldmindtest.repository.HomeRepository
import com.techcraeft.kinodaran.manager.HomeManager
import com.techcraeft.kinodaran.repository.util.Resource

class HomeManagerImpl(
    private val homeRepository: HomeRepository
) : HomeManager {

    override suspend fun getTopCategories(): Resource<Categories> =
        homeRepository.getTopCategories()

    override suspend fun getMostPopularProducts(): Resource<Products> =
        homeRepository.getMostPopularProducts()

    override suspend fun getProductById(productId: Int): Resource<ProductDetails> =
        homeRepository.getProductById(productId)
}