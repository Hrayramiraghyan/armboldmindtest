package com.techcraeft.kinodaran.manager

import com.example.armboldmindtest.model.Categories
import com.example.armboldmindtest.model.ProductDetails
import com.example.armboldmindtest.model.Products
import com.techcraeft.kinodaran.repository.util.Resource

interface HomeManager {
    suspend fun getTopCategories(): Resource<Categories>
    suspend fun getMostPopularProducts(): Resource<Products>
    suspend fun getProductById(productId: Int): Resource<ProductDetails>
}